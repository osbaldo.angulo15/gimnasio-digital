import React, {Component} from 'react';
import {Router as MainRouter} from './router';
import {Router} from 'react-router-dom';
import {navigator} from 'rrsx';
import {createMuiTheme} from '@material-ui/core/styles';
import {ThemeProvider, StylesProvider} from '@material-ui/styles';
import {GlobalTheme} from './globalTheme';

const theme = createMuiTheme(GlobalTheme);

class App extends Component {
  render() {
    return (
      <Router history={navigator}>
        <ThemeProvider theme={theme}>
          <StylesProvider injectFirst>
            <MainRouter />
          </StylesProvider>
        </ThemeProvider>
      </Router>
    );
  }
}

export default App;
