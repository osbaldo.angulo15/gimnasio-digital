// @flow
import React from 'react';
import {component, navigator} from 'rrsx';
import {Grid, Button, Typography} from '@material-ui/core';

function onClick() {
  navigator.push('sign-up');
}

type Props = { children: any, style: CSSStyleDeclaration, className: string };

const componentName = ({style, children, className}: Props) => {
  return <Grid>
    <Button variant="contained" color="primary" onClick={()=>onClick()}>
      <Typography variant="subtitle1" style={{color: '#ffffff'}}>
        HOLA
      </Typography>
    </Button>
  </Grid>;
};

export default component<Props>(componentName);
